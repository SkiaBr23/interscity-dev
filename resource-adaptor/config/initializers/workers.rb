if Rails.env.development? || Rails.env.production?
  WORKERS_LOGGER ||= Logger.new("#{Rails.root}/log/workers.log")

  $conn = Bunny.new(
    hostname: SERVICES_CONFIG['services']['rabbitmq'],
    logger: WORKERS_LOGGER,
  )
  $conn.start

end
