module SmartCities
  module CommandUpdater
    def update_command_status(command_id, status)
      command = ::ActuatorCommand.find(command_id)
      command.status = status
      command.save!
      WORKERS_LOGGER.info("ActuatorCommandUpdater::CommandUpdated - command=#{command_id}&status=#{status}")
    rescue StandardError => e
      WORKERS_LOGGER.error("ActuatorCommandUpdater::CommandNotUpdated - #{e.message}")
    end
  end
end
