require 'rubygems'
require 'json'
require 'rest-client'
require 'command_updater'
module SmartCities
  module CommandPublisher
    def publish_command(command)
      # Check for uuid and capability on command object
      uuid = command.uuid
      capability = command.capability
      if uuid && capability
        # Query subscriptions that match this uuid + capability
        subscriptions = ::Subscription.where(uuid: uuid, active: true)
        subscriptions.each do |subscription|
          # Asynchronously publish command on every subscription
          if subscription.capabilities.include? capability
            if subscription.type == 'webhook'
              ::WebHookPublisher.perform_async(subscription.id, subscription.url, command.to_json)
            elsif subscription.type == 'mqtt'
              ::MQTTPublisher.perform_async(subscription.id, command.to_json)
            end
          end
        end
      else
        raise 'UUID and Capability not provided'
      end
    end
  end
end
