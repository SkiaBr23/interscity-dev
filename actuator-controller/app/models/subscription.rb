require 'autoinc'

class Subscription < ApplicationRecord
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Autoinc
  include Filterable

  field :uuid, type: String
  field :_id, type: Integer
  field :url, type: String
  field :type, type: String
  field :capabilities, type: Array
  field :active, type: Boolean, default: true
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  # Override BSON id with incrementing integers
  increments :_id

  validates :uuid, :url, :type, :capabilities, presence: true
  validates :type, inclusion: {
    in: ['webhook', 'mqtt']
  }

  scope :uuid, ->(uuid) { where uuid: uuid }
  scope :type, ->(type) { where type: type }
  scope :active, ->(active) { where active: active }
  scope :recent, -> { order_by(created_at: 'desc') }

  # Change JSON output of "_id" attribute to "id"
  def as_json(*args)
    res = super
    res['id'] = res.delete('_id').to_s
    res
  end
end
