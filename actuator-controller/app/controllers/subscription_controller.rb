require 'json'

# Subscription Controller for CRUD operations on subscriptions
class SubscriptionController < ApplicationController
  before_action :set_page_params, only: [:index]
  before_action :set_subscription, only: [:show, :update, :destroy]

  # POST /subscriptions
  def create
    begin
      resource = PlatformResource.where(uuid: subscription_params['uuid']).first

      if resource.blank?
        render json: { error: 'Resource not found', uuid: subscription_params['uuid'] }, status: 404
        return false
      end

      return unless valid_capabilities?(resource, subscription_params)

      if subscription_params['type'] == 'mqtt'
        url = 'mqtt://' + request.remote_ip + ':1883'
      elsif subscription_params['type'] == 'webhook'
        url = subscription_params['url']
      else
        render json: { error: 'Invalid type! Choose between \'webhook\' and \'mqtt\'' }, status: 400
        return false
      end

      subscription_save(resource, subscription_params, url)
    rescue StandardError => e
      render json: { error: e.message }, status: 400
      return false
    end
  end

  # GET /subscriptions
  # GET /subscriptions?type=mqtt&uuid=1234&active=false
  def index
    @subscriptions = Subscription
                     .filter(params.slice(:uuid, :type, :active))
                     .recent.page(@page).per(@per_page)

    render json: { subscriptions: @subscriptions }, status: 200
  end

  def subscription_save(resource, subscription_params, url)
    subscription = Subscription.new(
      uuid: resource.uuid,
      capabilities: subscription_params['capabilities'],
      type: subscription_params['type'],
      url: url
    )

    if subscription.save
      render json: { subscription: subscription }, status: 201
    else
      render json: { error: "Invalid subscription #{subscription.errors.full_messages}" }, status: 404
    end
  end

  # PUT /subscriptions
  def update
    resource = PlatformResource.where(uuid: subscription_params['uuid']).first

    if resource.blank?
      render json: { error: 'Resource not found', uuid: subscription_params['uuid'] }, status: 404
      return false
    end

    return unless valid_capabilities?(resource, subscription_params)

    if @subscription.update(subscription_params)
      render json: { subscription: @subscription }, status: 200
    else
      render json: { errors: @subscription.errors }, status: 422
    end
  end

  # DELETE /subscriptions
  def destroy
    @subscription.destroy
    redirect_to action: 'index', status: 204
  end

  # GET /subscriptions/:id
  def show
    render json: { subscription: @subscription }, status: 200
  end

  def valid_capabilities?(resource, subscription_params)
    subscription_params['capabilities'].each do |capability|
      unless resource.capabilities.include? capability
        render json: { error: 'This resource does not have such capability',
                       uuid: resource.uuid,
                       capability: capability }, status: 400
        return false
      end
    end
    return true
  end

  private

  def set_page_params
    @page = params[:page].blank? ? 1 : params[:page].to_i
    @per_page = params[:per_page].blank? ? 40 : params[:per_page].to_i
  end

  def set_subscription
    begin
      @subscription = Subscription.find(params[:id])
    rescue Mongoid::Errors::DocumentNotFound
      render json: { error: 'Subscription not found' }, status: 404
      return
    end
  end

  def subscription_params
    params.require(:subscription).permit(:uuid, :url, :type, capabilities: [])
  end
end
