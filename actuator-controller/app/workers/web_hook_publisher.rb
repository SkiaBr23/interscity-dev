require 'rubygems'
require 'json'
require 'rest-client'
require 'command_updater'

class WebHookPublisher
  include Sidekiq::Worker
  sidekiq_options queue: 'web_hook_caller', backtrace: true

  include SmartCities::CommandUpdater

  def perform(id, url, command_json)
    begin
      # Parse command json to access attributes
      command = JSON.parse(command_json)
      # POST request on the given URL
      RestClient.post(
        url,
        { action: 'actuator_command', command: command }.to_json,
        { content_type: :json, accept: :json }
      )
      WORKERS_LOGGER.info("Publisher::Webhook CommandSent - notification_id: #{id},
                           uuid: #{command['uuid']},
                           url: #{url}")
      # Update command status with 'processed'
      update_command_status(command['_id']['$oid'], 'processed')
    rescue RestClient::ExceptionWithResponse => e
      WORKERS_LOGGER.error("Publisher::Webhook CommandNotSent - notification_id: #{id},
                            url: #{url},
                            uuid: #{command['uuid']},
                            error: #{e.message}")
      # Update command status with 'rejected'
      update_command_status(command['_id']['$oid'], 'rejected')
    rescue StandardError => e
      WORKERS_LOGGER.error("Publisher::Webhook CommandNotSent - notification_id: #{id},
                            url: #{url},
                            uuid: #{command['uuid']},
                            error: #{e.message}")
      # Update command status with 'failed'
      update_command_status(command['_id']['$oid'], 'failed')
      raise e # This will make sidekiq to retry again later
    end
  end
end
