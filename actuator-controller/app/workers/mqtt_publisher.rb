require 'rubygems'
require 'json'
require 'rest-client'
require 'command_updater'

class MQTTPublisher
  include Sidekiq::Worker
  sidekiq_options queue: 'mqtt_publisher', backtrace: true

  include SmartCities::CommandUpdater

  def perform(id, command_json)
    # Get command json as message and parse to get attributes
    message = command_json
    command = JSON.parse(command_json)
    # Build topic name with Resource ID and capability name
    topic = "actuator/#{command['uuid']}/capability/#{command['capability']}"
    begin
      # Serialize command to JSON and publish as message
      $mqtt.publish(topic, message)
      # Log publish event
      WORKERS_LOGGER.info("Publisher::MQTT CommandSent - notification_id: #{id}, uuid: #{command['uuid']}")
      # Update command with 'processed' status
      update_command_status(command['_id']['$oid'], 'processed')
    rescue MQTT::NotConnectedException => e
      WORKERS_LOGGER.info("Publisher::MQTT CommandNotSent - notification_id: #{id},
                           uuid: #{command['uuid']},
                           error: #{e.message}")
      update_command_status(command['_id']['$oid'], 'failed')
      $mqtt.connect
      raise e
    end
  end
end
