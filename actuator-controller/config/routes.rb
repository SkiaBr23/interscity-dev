require 'sidekiq/web'
Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Health Check endpoint
  get 'health_check', to: 'health_check#index'

  # Command endpoints
  post '/commands', to: 'actuator#create'
  get '/commands', to: 'actuator#index'

  # Subscription endpoints
  post '/subscriptions', to: 'subscription#create'
  get '/subscriptions', to: 'subscription#index'
  put '/subscriptions/:id', to:'subscription#update'
  delete '/subscriptions/:id',to:'subscription#destroy'
  get '/subscriptions/:id',to:'subscription#show'

end
